'use strict';
const bcrypt = require('bcrypt');
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    
    const salt = bcrypt.genSaltSync(11);
    const hashPassword = bcrypt.hashSync('pororogevU29', salt);

    await queryInterface.bulkInsert('Users', [{
      username: 'celespetrus',
      email: 'celespratama@gmail.com',
      password: hashPassword,
      role: 'admin',
      user_type: 'basic',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});

  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */

     await queryInterface.bulkDelete('People', null, {});
  }
};
