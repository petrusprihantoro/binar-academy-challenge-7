const express = require('express');
const router = express.Router();
const userController = require('../controllers/user')
const token = require('../middlewares/token')
const role = require('../middlewares/role')

router.get('/auth/google', userController.googleSignIn);
router.post('/auth/sign_in', userController.signIn);
router.post('/auth/login', userController.login);
router.put('/update', token.tokenCheck ,userController.updateProfile);
router.delete('/:id', token.tokenCheck, role.admin ,userController.deleteUser);
router.post('/auth/forgot_password', userController.forgotPassword);
router.post('/auth/reset_password/:id', userController.resetPassword);

module.exports = router;
