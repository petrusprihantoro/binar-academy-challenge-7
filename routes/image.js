const express = require('express');
const router = express.Router();
const imageController = require('../controllers/image')
const token = require('../middlewares/token')
const role = require('../middlewares/role')
const storage = require('../middlewares/storage')

router.post('/avatar', token.tokenCheck, role.user, storage.uploadAvatar.single('avatar'), imageController.updateAvatar);

module.exports = router;
