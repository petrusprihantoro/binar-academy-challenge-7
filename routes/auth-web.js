const express = require('express');
const router = express.Router();

router.get('/auth/login', (req,res) => {
    res.render('login');
})

router.get('/auth/sign_in', (req,res) => {
    res.render('sign_in');
})

router.get('/auth/forgot_password', (req,res) => {
    res.render('forgot_password');
})

router.get('/auth/reset_password/:id', (req,res) => {
    const id = req.params.id;
    const token = req.query.token;
    res.render('reset_password', {id, token});
})

module.exports = router;