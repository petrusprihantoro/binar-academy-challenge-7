const express = require('express');
const router = express.Router();

const user = require('./user');
const image = require('./image');
const video = require('./video');
const page = require('./auth-web');

router.get('/documentation', (req,res) => {
    res.redirect("https://documenter.getpostman.com/view/21188415/Uz5NiCfR")
})
router.use('/user', user);
router.use('/image', image);
router.use('/video', video);
router.use('/page', page);

module.exports = router;
