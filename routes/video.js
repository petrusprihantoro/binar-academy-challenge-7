const express = require('express');
const router = express.Router();
const videoController = require('../controllers/video')
const token = require('../middlewares/token')
const role = require('../middlewares/role')
const storage = require('../middlewares/storage')

router.post('/post', token.tokenCheck, role.user, storage.uploadVideo.single('video'), videoController.uploadVideo);
router.get('/', token.tokenCheck, role.admin, videoController.getVideos);
router.delete('/:id', token.tokenCheck, role.admin, videoController.deleteVideo);
module.exports = router;
