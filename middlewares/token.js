const {User} = require('../models')
const jwt = require('jsonwebtoken')
module.exports = {
    tokenCheck: async (req,res,next) => {
        try {
            if (!req.headers.token) {
                return res.status(401).json({
                    status: false,
                    message: 'token is required!',
                    data: null
                });
            }
            const token = req.headers.token;
            const decoded = jwt.verify(token, process.env.MY_SECRET_KEY);
            const user = decoded;
            const userData = await User.findOne({
                where: {
                    email:user.email
                }
            })

            req.user = {
                id:userData.id,
                username:userData.username,
                email:userData.email,
                role:userData.role
            }
            // console.log(user.email);
            next();
        } catch (err) {
            return res.status(500).json({
                status: false,
                message: err.message,
                data: null
            });
        }
    }
}