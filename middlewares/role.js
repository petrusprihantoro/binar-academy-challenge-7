module.exports = {
    admin: (req,res,next) => {
        try {
            if (!req.user || req.user.role != 'admin'){
                return res.status(401).json({
                    status: false,
                    message: 'only role admin can access this endpoint!',
                    data: null
                });
            }
            next();
        } catch (err) {
            return res.status(500).json({
                status: false,
                message: err.message,
                data: null
            });
        }
    },
    user: (req,res,next) => {
        try {
            if (!req.user || req.user.role != 'user'){
                return res.status(401).json({
                    status: false,
                    message: 'only role user can access this endpoint!',
                    data: null
                });
            }
            next();
        } catch (err) {
            return res.status(500).json({
                status: false,
                message: err.message,
                data: null
            });
        }
    }
}