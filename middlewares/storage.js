const multer = require('multer');
const path = require('path');

const storage = (type) => {    
    return multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, `./public/${type}`);
    },

    filename: function (req, file, callback) {
        const namaFile = type + '-' + Date.now() + path.extname(file.originalname);
        callback(null, namaFile);
    }
});
}
    


const uploadAvatar = multer({
    storage: storage('avatars'),
    fileFilter: (req, file, callback) => {
        if (file.mimetype == 'image/png' || file.mimetype == 'image/jpg' || file.mimetype == 'image/jpeg') {
            callback(null, true);
        } else {
            callback(null, false);
            callback(new Error('only png, jpg, and jped allowed to upload!'));
        }
    },
    onError: function (err, next) {
        console.log('error', err);
        next(err);
    }
});

const uploadVideo = multer({
    storage: storage('videos'),
    fileFilter: (req, file, callback) => {
        console.log(file.mimetype);
        if (file.mimetype == 'video/mp4') {
            callback(null, true);
        } else {
            callback(null, false);
            callback(new Error('only mp4 allowed to upload!'));
        }
    },
    onError: function (err, next) {
        console.log('error', err);
        next(err);
    }
});

const uploadFile = multer({
    storage: storage('files'),
    fileFilter: (req, file, callback) => {
        if (file.mimetype == 'application/pdf' || file.mimetype == 'application/msword' || file.mimetype == 'application/vnd.ms-excel') {
            callback(null, true);
        } else {
            callback(null, false);
            callback(new Error('only pdf, docs, and xls are allowed to upload!'));
        }
    },
    onError: function (err, next) {
        console.log('error', err);
        next(err);
    }
});

module.exports = {
    uploadAvatar,
    uploadFile,
    uploadVideo
};