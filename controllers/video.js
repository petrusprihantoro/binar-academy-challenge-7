const {Video} = require('../models');

const uploadVideo = async (req,res) => {
    try {
        const videoUrl = req.protocol + '://' + req.get('host') + '/videos/' + req.file.filename;

        if (!req.body.caption) {
            return res.respondBadRequest('caption is required');
        }

        const newVideo = await Video.create({
            user_id: req.user.id,
            url: videoUrl,
            caption: req.body.caption
        })

        res.respondSuccess(newVideo, 'video uploaded successfully');
    } catch (err) {
        res.respondServerError(err.message);
    }
}

const getVideos = async (req,res) => {
    try {
        const videos = await Video.findAll();

        res.respondSuccess(videos, 'videos retrieved successfully');
    } catch (err) {
        res.respondServerError(err.message);
    }
}

const deleteVideo = async (req,res) => {
    try {
        const video_id = req.params.id;

        const video = await Video.findOne({ where: { id: video_id } });

        if (!video) {
            return res.respondNotFound('video not found');
        }

        await video.destroy();

        res.respondSuccess(null, 'video deleted successfully');
    } catch (err) {
        res.respondServerError(err.message);
    }
}

module.exports = {
    uploadVideo,
    getVideos,
    deleteVideo
}