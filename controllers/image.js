const multer = require('multer');
const {Image, User} = require('../models');

const updateAvatar = async (req,res) => {
    try {
        const imageUrl = req.protocol + '://' + req.get('host') + '/avatars/' + req.file.filename;

        const user_id = req.user.id;

        const image = await Image.create({
            title: `avatar-${user_id}`,
            url: imageUrl,
            alt: 'avatar'
        })

        await User.update(
            {
                avatar: image.id
            },
            {
                where: {
                    id:user_id
                }
            }
        )

        res.status(200).json({
            message:'success',
            data:image
        })
    } catch (err) {
        return res.status(500).json({
            status: false,
            message: err.message,
            data: null
        });
    }
}

const updateVideo = async (req,res) => {
    try {
        const videoUrl = req.protocol + '://' + req.get('host') + '/videos/' + req.file.filename;

        res.status(200).json({
            status: true,
            message: "success",
            data: videoUrl
        });
    } catch (err) {
        return res.status(500).json({
            status: false,
            message: err.message,
            data: null
        });
    }
}

const updateFiles = async (req,res) => {
    try {
        const fileUrl = req.protocol + '://' + req.get('host') + '/files/' + req.file.filename;

        res.status(200).json({
            status: true,
            message: "success",
            data: fileUrl
        });
    } catch (err) {
        return res.status(500).json({
            status: false,
            message: err.message,
            data: null
        });
    }
}

module.exports = {
    updateAvatar
}