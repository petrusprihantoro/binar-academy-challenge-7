require('dotenv').config();
const bcrypt = require('bcrypt');
const { google } = require('googleapis');
const {Op} = require('sequelize');
const {User} = require('../models');
const Validator = require('fastest-validator');
const v = new Validator();
const jwt = require('jsonwebtoken');
const emailHelpers = require('../helpers/email');
const nodemailer = require('nodemailer');
const {
    GOOGLE_CLIENT_ID,
    GOOGLE_CLIENT_SECRET,
    APP_DOMAIN,
    GOOGLE_REDIRECT_URI,
    GOOGLE_MAIL_REFRESH_TOKEN,
    GOOGLE_MAIL_SENDER,
    GOOGLE_MAIL_REDIRECT_URI
} = process.env

const oAuth2Client = new google.auth.OAuth2(
    GOOGLE_CLIENT_ID,
    GOOGLE_CLIENT_SECRET,
    `${APP_DOMAIN}/${GOOGLE_REDIRECT_URI}`
)

const oAuth2EmailSender = new google.auth.OAuth2(
    GOOGLE_CLIENT_ID,
    GOOGLE_CLIENT_SECRET,
    GOOGLE_MAIL_REDIRECT_URI
)

oAuth2EmailSender.setCredentials({ refresh_token: GOOGLE_MAIL_REFRESH_TOKEN });

const sendEmail = async (to,subject,html) => {
    try {
        const accessToken = await oAuth2EmailSender.getAccessToken();
        // console.log(accessToken);
        const transport = nodemailer.createTransport({
            service:'gmail',
            auth: {
                type: 'OAuth2',
                user: GOOGLE_MAIL_SENDER,
                clientId: GOOGLE_CLIENT_ID,
                clientSecret: GOOGLE_CLIENT_SECRET,
                refreshToken: GOOGLE_MAIL_REFRESH_TOKEN,
                accessToken
            }
        })
        console.log(transport);
        const mailOptions = {
            to, subject, html
        };

        const response = await transport.sendMail(mailOptions);
        return response
    } catch (err) {
        return err
    }
}

const signIn = async (req,res) => {
    try {
        const schema = {
            username: 'string|min:3|max:255',
            email: 'email',
            password: 'string'
        };

        const validate = v.validate(req.body, schema);

        if (validate.length) {
            return res.respondBadRequest(validate);
        }

        const {username,email,password} = req.body
    
        const salt = bcrypt.genSaltSync(11);
        const hashPassword = bcrypt.hashSync(password, salt);
    
        const [newUser, isCreated] = await User.findOrCreate({
            where: {
                [Op.or]: [{username}, {email}]
            },
            defaults: {
                username,
                email,
                password:hashPassword,
                role: 'user',
                user_type: 'basic'
            }
        })
    
        if (!isCreated) {
            return res.respondBadRequest('username or email already exist');
        }

        const html = await emailHelpers.getHtml('greetings', {username});

        const response = await sendEmail(email, "Welcome", html);

        res.respondCreated({username: newUser.username}, 'sign in success');
    } catch (err) {
        res.respondServerError(err.message);
    }
}

const login = async (req,res) => {
    try {
        const {username, email, password} = req.body;

        if (!(username || email) || !password) {
            return res.status(404).json({
                message:'invalid login'
            })
        }

        const user = await User.authenticate(req.body);

        // console.log(user);
        
        const accessToken = user.generateToken();

        const data = {
            id: user.id,
            email: user.email,
            token: accessToken
        }
        res.respondSuccess(data, 'login success');
    } catch (err) {
        res.respondServerError(err.message);
    }
}

const generateAuthUrl = () => {
    const scopes = [
        'https://www.googleapis.com/auth/userinfo.email',
        'https://www.googleapis.com/auth/userinfo.profile'
    ];

    const authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: scopes,
        response_type: 'code'
    });

    return authUrl
}

const setCredentials = async (code) => {
    return new Promise(async (resolve, rejects) => {
        try {
            const { tokens } = await oAuth2Client.getToken(code);
            oAuth2Client.setCredentials(tokens);

            return resolve(tokens);
        } catch (err) {
            return rejects(err);
        }
    });
}

const getUserInfo = async () => {
    return new Promise(async (resolve, rejects) => {
        try {
            const oAuth2 = google.oauth2({
                auth: oAuth2Client,
                version:'v2'
            });

            const data = oAuth2.userinfo.get((err,res) => {
                if (err) {
                    return rejects(err);
                }
                return resolve(res);
            });
        } catch (err) {
            return rejects(err);
        }
    })
}

const googleSignIn = async (req,res) => {
    try {
        const code = req.query.code
    
        if (!code) {
            const loginUrl = generateAuthUrl();
            return res.redirect(loginUrl);
        }
    
        await setCredentials(code);
    
        const {data} = await getUserInfo();
    
        const [newUser] = await User.findOrCreate({
            where: {
                email:data.email
            },
            defaults: {
                email:data.email,
                user_type:'google',
                role:'user'
            }
        })
    
        const accessToken = newUser.generateToken()

        const userData ={
            id:newUser.dataValues.id,
            email:newUser.dataValues.email,
            role:newUser.dataValues.role,
            user_type:newUser.dataValues.user_type,
            token:accessToken
        }

        res.respondSuccess(userData, 'google sign in success');
    } catch (err) {
        res.respondServerError(err.message);
    }
}

const updateProfile = async (req,res) => {
    try {
        const user_id = req.user.id;

        const user = await User.findOne({ where: { id: user_id } });

        if (!user) {
            res.respondNotFound(`can't find user with id ${user_id}!`);
            return;
        };

        const schema = {
            username: 'string|min:3|max:255|optional',
            email: 'email|optional'
        };

        const validate = v.validate(req.body, schema);
        if (validate.length) {
            res.respondBadRequest(validate);
            return;
        }

        const {username, email} = req.body;

        const updated = await User.update(
            {
                username, email
            },
            {
                where: { id: user_id }
            });

        res.respondUpdated(updated);
    } catch (err) {
        res.respondServerError(err.message);
    }
}

const forgotPassword = async (req,res) => {
    try {
        const email = req.body.email;
    
        const user = await User.findOne({
            where: {
                email
            }
        })
    
        if (!user) {
            return res.respondNotFound('email doesn\'t exist')
        }
    
        const jwtPayload = {
            id:user.id,
            username:user.username,
            email:user.email
        }
        
        const secret = process.env.MY_SECRET_KEY + user.password;
    
        const token = jwt.sign(jwtPayload, secret,  {expiresIn: '5m' })
    
        const html = await emailHelpers.getHtml('reset_password', {
            id:user.id,
            token:token
        })
        const response = await sendEmail(email, "MyApp Reset Password", html);

        return res.respondSuccess(null, 'email sent')
    } catch (err) {
        res.respondServerError(err.message);
    }
}

const resetPassword = async (req,res) => {
    try {
        const id = req.params.id;
    
        const user = await User.findOne({
            where: {
                id
            }
        })
        const token = req.query.token;
        const jwtSecret = process.env.MY_SECRET_KEY + user.password;
        const data = jwt.verify(token,jwtSecret);
        console.log(data);
        const {newPassword, newPasswordConfirm} = req.body;
        
        if (newPassword != newPasswordConfirm) {
            return res.respondBadRequest("invalid input")
        }
    
        const hashPassword = bcrypt.hashSync(newPassword, bcrypt.genSaltSync(11));
    
        await User.update({
            password:hashPassword
        }, {
            where: {
                email:data.email
            }
        })
    
        return res.respondUpdated();
    } catch (err) {
        return res.respondServerError(err.message);
    }
}

const deleteUser = async (req, res) => {
    try {
        const user_id = req.params.id;

        const user = await User.findOne({ where: { id: user_id } });

        if (!user) {
            res.respondNotFound(`can't find user with id ${user_id}!`);
            return;
        };

        const deleted = await User.destroy({ where: { id: user_id } });

        res.respondDeleted(deleted);

    } catch (err) {
        res.respondServerError(err.message);
    }
}

module.exports = {
    signIn,
    login,
    googleSignIn,
    updateProfile,
    deleteUser,
    forgotPassword,
    resetPassword
}