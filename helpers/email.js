const ejs = require('ejs');
const path = require('path');
module.exports = {
    getHtml: (filename, data) => {
        return new Promise((resolve, reject) => {
            const pathData = path.join(__dirname+`/../views/templates/${filename}.ejs`)
            ejs.renderFile(pathData, data, (err,data) => {
                if (err) {
                    return reject(err)
                }
                return resolve(data)
            })
        })
    }
}