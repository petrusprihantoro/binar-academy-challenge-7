const Sentry = require('@sentry/node');

Sentry.init({
    dsn: "https://25b968dbf8234bd8b28af0d98f9c6fe9@o1253693.ingest.sentry.io/6426703"
});

module.exports = Sentry;