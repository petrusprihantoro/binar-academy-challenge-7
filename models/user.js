'use strict';
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {
  Model
} = require('sequelize');
const {Op} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.belongsTo(models.Image, {foreignKey: 'avatar'});
      User.hasMany(models.Video, {foreignKey: 'user_id', as: "videos"});
    }

    generateToken = () => {
      const payload = {
        id:this.id,
        username:this.username,
        email:this.email
      }

      const jwtSecret = process.env.MY_SECRET_KEY;

      return jwt.sign(payload, jwtSecret);
    }

    checkPassword = (password) => {
      return bcrypt.compareSync(password, this.password);
    }

    static authenticate = async ({username=null,email=null, password}) => {
      try {
        const user = await this.findOne({
          where: {
            [Op.or]:[{username}, {email}]
          }
        });

        if (!user) return Promise.reject(new Error('user not found!'));
        const isPasswordValid = user.checkPassword(password);
        if (!isPasswordValid) return Promise.reject(new Error('invalid password'));

        return Promise.resolve(user)
      } catch (err) {
        return Promise.reject(err)
      }
    }
    
  }
  User.init({
    username: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    avatar: DataTypes.BIGINT,
    role: DataTypes.STRING,
    user_type: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};